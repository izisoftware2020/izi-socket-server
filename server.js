console.log("Server JS");

// use express
var express = require("express");

// create instance of express
var app = express();
const request = require("request");

// use http with instance of express
var http = require("http").createServer(app);

// create socket instance with http
var io = require("socket.io")(http, {
  allowEIO3: true, // f,
  cors: {
    origin: "*",
    credentials: true,
  },
});

const ioClient = require("socket.io-client");
const socketClient = ioClient.connect("wss://socket1.crudcode.tk");

// add listener for new connection
io.on("connection", function (socket) {
  // this is socket for each user
  console.log("User connected", socket.id);

  // server should listen from each client via it's socket
  socket.on("new_message", function (data) {
    console.log("Client says", data);

    // server will send message to all connection client
    // send same message back to all user
    io.emit("new_message", data);
  });

  // server for chat private
  socket.on("new_message_private", function (data) {
    console.log("Client says", data);

    // server will send message to all connection client
    // send same message back to all user
    io.emit("new_message_private", data);
  });

  // server for signal booking car
  socket.on("booking_car_new_signal", function (data) {
    console.log("Client says", data);

    // server will send message to all connection client
    // send same message back to all user
    io.emit("booking_car_new_signal", data);
  });

  // server for notification booking car
  socket.on("booking_car_new_notification", function (data) {
    console.log("Client says", data);

    // server will send message to all connection client
    // send same message back to all user
    io.emit("booking_car_new_notification", data);
  });

  // server for notification booking car
  socket.on("booking_car_new_login", function (data) {
    console.log("Client says", data);

    // server will send message to all connection client
    // send same message back to all user
    io.emit("booking_car_new_login", data);
  });

  // server for notification booking car
  socket.on("booking_car_delete_trip", function (data) {
    console.log("Client says", data);

    // server will send message to all connection client
    // send same message back to all user
    io.emit("booking_car_delete_trip", data);
  });

  // server for notification booking car
  socket.on("cancel_signal_driver", function (data) {
    console.log("Client says", data);

    // server will send message to all connection client
    // send same message back to all user
    io.emit("cancel_signal_driver", data);
  });

  // server for notification booking car
  socket.on("p09fivess", function (data) {
    console.log("Client says", data);

    // server will send message to all connection client
    // send same message back to all user
    io.emit("p09fivess", data);
  });

  // server for notification booking car
  socket.on("p09fivess_payment", function (data) {
    console.log("Client says", data);

    // server will send message to all connection client
    // send same message back to all user
    io.emit("p09fivess_payment", data);
  });

  // server for notification booking car
  socket.on("p03familycar_refresh", function (data) {
    console.log("Client says p03familycar_refresh", data);

    // server will send message to all connection client
    // send same message back to all user
    io.emit("p03familycar_refresh", data);
  });

  // server for notification booking car
  socket.on("p03familycar_online", function (data) {
    console.log(
      "User này đang đăng nhập rồi update vào database",
      data,
      socket.id
    );

    // call api php cập nhật user thành online
    const param = {
      what: "630",
      IdPerson: data,
      SocketId: socket.id,
    };

    request(
      {
        url: "https://api.adminfamilycar.com/p6bookingcar/P1Controller/C3Mobile/SelectAllByWhat.php",
        method: "POST",
        json: true,
        body: param,
      },
      function (error, response, body) {
        // console.log('phuong', response.statusCode, response.body);
        if (!error && response.statusCode == 200) {
          // console.log(response.body);

          // push user online
          socketClient.emit("p03familycar_refresh", socket.id);
          console.log(
            "push online p03familycar_refreshp03familycar_refreshp03familycar_refreshp03familycar_refreshp03familycar_refreshp03familycar_refreshp03familycar_refresh"
          );
        }
      }
    );

    // server will send message to all connection client
    // send same message back to all user
    io.emit("p03familycar_online", data);
  });

  // server for notification booking car
  socket.on("p03familycar_offline", function (data) {
    console.log("Client says p03familycar_offline", data);

    // server will send message to all connection client
    // send same message back to all user
    io.emit("p03familycar_offline", data);
  });

  // server for notification booking car
  socket.on("p03familycar_update_status_signal", function (data) {
    console.log("Client says p03familycar_update_status_signal", data);

    // server will send message to all connection client
    // send same message back to all user
    io.emit("p03familycar_update_status_signal", data);
  });

  // DISCONNECT EVENT
  socket.on("disconnect", (reason) => {
    // cập nhật user có socket.id thành offline
    console.log("User đã disconnect rồi", socket.id);
    // call api php cập nhật user thành offline
    const param = {
      what: "631",
      SocketId: socket.id,
    };

    request(
      {
        url: "https://api.adminfamilycar.com/p6bookingcar/P1Controller/C3Mobile/SelectAllByWhat.php",
        method: "POST",
        json: true,
        body: param,
      },
      function (error, response, body) {
        if (!error && response.statusCode == 200) {
          // push user offline
          socketClient.emit("p03familycar_refresh", socket.id);

          socket.disconnect(); // DISCONNECT SOCKET
          // console.log(response.body);
        }
      }
    );

    console.log("Is Offline!", socket.id);
  });

  // server for notification booking car
  socket.on("driver_reciever_singal", function (data) {
    console.log("Client says driver_reciever_singal", data);

    // server will send message to all connection client
    // send same message back to all user
    io.emit("driver_reciever_singal", data);
  });

  // server for notification booking car
  socket.on("finish_booking", function (data) {
    console.log("Client says finish_booking", data);

    // server will send message to all connection client
    // send same message back to all user
    io.emit("finish_booking", data);
  });

  // server for notification booking car
  socket.on("emc", function (data) {
    console.log("Client says emc", data);

    // server will send message to all connection client
    // send same message back to all user
    io.emit("emc", data);
  });

  // server for notification booking car
  socket.on("emc_sent", function (data) {
    console.log("Client says emc", data);

    // server will send message to all connection client
    // send same message back to all user
    io.emit("emc_sent", data);
  });

  // server for notification booking car
  socket.on("p24ecommerce_chat", function (data) {
    console.log("Client says emc", data);

    // server will send message to all connection client
    // send same message back to all user
    io.emit("p24ecommerce_chat", data);
  });

  // server for notification booking car
  socket.on("p23dpfood_socket", function (data) {
    console.log("Client says emc", data);

    // server will send message to all connection client
    // send same message back to all user
    io.emit("p23dpfood_socket", data);
  });

  // server for notification fooding
  socket.on("p23dpfood_offline_socket", function (data) {
    console.log("Client says emc", data);

    // server will send message to all connection client
    // send same message back to all user
    io.emit("p23dpfood_offline_socket", data);
  });

  // server for notification fooding
  socket.on("p23dpfood_online_socket", function (data) {
    console.log("Client says emc", data);

    // server will send message to all connection client
    // send same message back to all user
    io.emit("p23dpfood_online_socket", data);
  });

  // server for notification booking car
  socket.on("p29fooding_socket", function (data) {
    console.log("Client says emc", data);

    // server will send message to all connection client
    // send same message back to all user
    io.emit("p29fooding_socket", data);
  });

  // server for notification fooding
  socket.on("p29fooding_online_socket", function (data) {
    console.log("Client says emc", data);

    // server will send message to all connection client
    // send same message back to all user
    io.emit("p29fooding_online_socket", data);
  });

  // server for notification fooding
  socket.on("p29fooding_offline_socket", function (data) {
    console.log("Client says emc", data);

    // server will send message to all connection client
    // send same message back to all user
    io.emit("p29fooding_offline_socket", data);
  });

  // server for notification booking car
  socket.on("ecommerce_socket", function (data) {
    console.log("Client says emc", data);

    // server will send message to all connection client
    // send same message back to all user
    io.emit("ecommerce_socket", data);
  });

  // server for notification p40_flag
  socket.on("p40_flag", function (data) {
    console.log("p40_flag says emc", data);

    // server will send message to all connection client
    // send same message back to all user
    io.emit("p40_flag", data);
  });

  // server for notification p40_flag_play_realtime
  socket.on("p40_flag_play_realtime", function (data) {
    console.log("Client says emc", data);

    // server will send message to all connection client
    // send same message back to all user
    io.emit("p40_flag_play_realtime", data);
  });

  // server for notification p37_booking_order
  socket.on("p37_booking_order", function (data) {
    console.log("Client says emc", data);

    // server will send message to all connection client
    // send same message back to all user
    io.emit("p37_booking_order", data);
  });

  // server for notification p45_bobe_order
  socket.on("p45_bobe_order", function (data) {
    console.log("Client says emc", data);

    // server will send message to all connection client
    // send same message back to all user
    io.emit("p45_bobe_order", data);
  });

  // server for notification p45_hiyou_support
  socket.on("p45_hiyou_support", function (data) {
    console.log("Client says emc", data);

    // server will send message to all connection client
    // send same message back to all user
    io.emit("p45_hiyou_support", data);
  });

    // server for notification p45hiyou_new_commission_change
    socket.on("p45hiyou_new_commission_change", function (data) {
      console.log("Client says emc", data);
  
      // server will send message to all connection client
      // send same message back to all user
      io.emit("p45hiyou_new_commission_change", data);
    });

  // server for notification p21_haqua_socket
  socket.on("p21_haqua_socket", function (data) {
    console.log("Client says emc", data);

    // server will send message to all connection client
    // send same message back to all user
    io.emit("p21_haqua_socket", data);
  });

  // server for notification p48_dating_socket
  socket.on("p48_dating_socket", function (data) {
    console.log("Client says emc", data);

    // server will send message to all connection client
    // send same message back to all user
    io.emit("p48_dating_socket", data);
  });

  // server for notification p48datting_comment
  socket.on("p48datting_comment", function (data) {
    console.log("Client says emc", data);

    // server will send message to all connection client
    // send same message back to all user
    io.emit("p48datting_comment", data);
  });

  // server for notification p48datting_comment-typing
  socket.on("p48datting_comment-typing", function (data) {
    console.log("Client says emc", data);

    // server will send message to all connection client
    // send same message back to all user
    io.emit("p48datting_comment-typing", data);
  });

  // server for notification p48datting_reply_comment
  socket.on("p48datting_reply_comment", function (data) {
    console.log("Client says emc", data);

    // server will send message to all connection client
    // send same message back to all user
    io.emit("p48datting_reply_comment", data);
  });

  // server for notification p48datting_reply_comment_typing
  socket.on("p48datting_reply_comment_typing", function (data) {
    console.log("Client says emc", data);

    // server will send message to all connection client
    // send same message back to all user
    io.emit("p48datting_reply_comment_typing", data);
  });

  // server for notification p48datting_join_chat
  socket.on("p48datting_join_chat", function (data) {
    console.log("Client says emc", data);

    // server will send message to all connection client
    // send same message back to all user
    io.emit("p48datting_join_chat", data);
  });

  // server for notification p48datting_new_message
  socket.on("p48datting_new_message", function (data) {
    console.log("Client says emc", data);

    // server will send message to all connection client
    // send same message back to all user
    io.emit("p48datting_new_message", data);
  });

  // server for notification p48datting_received_message
  socket.on("p48datting_received_message", function (data) {
    console.log("Client says emc", data);

    // server will send message to all connection client
    // send same message back to all user
    io.emit("p48datting_received_message", data);
  });

  // server for notification p48datting_story_uploaded
  socket.on("p48datting_story_uploaded", function (data) {
    console.log("Client says emc", data);

    // server will send message to all connection client
    // send same message back to all user
    io.emit("p48datting_story_uploaded", data);
  });

  // server for notification p48datting_post
  socket.on("p48datting_post", function (data) {
    console.log("Client says emc", data);

    // server will send message to all connection client
    // send same message back to all user
    io.emit("p48datting_post", data);
  });

  // server for notification p48datting_donate_live_stream
  socket.on("p48datting_donate_live_stream", function (data) {
    console.log("Client says emc", data);

    // server will send message to all connection client
    // send same message back to all user
    io.emit("p48datting_donate_live_stream", data);
  });

  // server for notification p48datting_live_audience_activity
  socket.on("p48datting_live_audience_activity", function (data) {
    console.log("Client says emc", data);

    // server will send message to all connection client
    // send same message back to all user
    io.emit("p48datting_live_audience_activity", data);
  });
  // server for notification p48datting_count_live_viewers
  socket.on("p48datting_count_live_viewers", function (data) {
    console.log("Client says emc", data);

    // server will send message to all connection client
    // send same message back to all user
    io.emit("p48datting_count_live_viewers", data);
  });

   // server for notification p63_tienbooking_order
   socket.on("p63_tienbooking_order", function (data) {
    console.log("Client says emc", data);

    // server will send message to all connection client
    // send same message back to all user
    io.emit("p63_tienbooking_order", data);
  });

  // server for notification p63_tienbooking_order
  socket.on("p63_tienbooking_support", function (data) {
    console.log("Client says emc", data);

    // server will send message to all connection client
    // send same message back to all user
    io.emit("p63_tienbooking_support", data);
  });

    // server for notification p63_tienbooking_new_commission_change
    socket.on("p63_tienbooking_new_commission_change", function (data) {
      console.log("Client says emc", data);
  
      // server will send message to all connection client
      // send same message back to all user
      io.emit("p63_tienbooking_new_commission_change", data);
    });
});

app.get("/", function (request, result) {});

// start the server
var port = 34567;
http.listen(port, function () {
  console.log("Listening to port " + port);
});
